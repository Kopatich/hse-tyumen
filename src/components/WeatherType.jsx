import React from 'react';
import { Grid, Typography } from '@mui/material';
import { QuestionMark } from '@mui/icons-material';

function WeatherType({ type, icon }) {
	return (
		<Grid container spacing={1}>
			<Grid item xs={12}>
				<Typography sx={{ textAlign: 'right' }} variant="body1">
					{type}
				</Typography>
			</Grid>
			<Grid
				item
				sx={{ display: 'flex', justifyContent: 'flex-end' }}
				xs={12}
			>
				{icon ? <img src={`http:${icon}`} alt="" /> : <QuestionMark />}
			</Grid>
		</Grid>
	);
}

export default WeatherType;
