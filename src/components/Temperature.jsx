import React from 'react';
import Typography from '@mui/material/Typography';

function Temperature(props) {
	return (
		<Typography variant="h4">
			{props.temp ? <>{props.temp} &deg;C</> : 'N/A'}
		</Typography>
	);
}

export default Temperature;
