import React from 'react';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import { ArrowUpward, WaterDrop, WindPower } from '@mui/icons-material';
import { Typography } from '@mui/material';

const parameterToIcon = {
	windDirection: ArrowUpward,
	windSpeed: WindPower,
	humidity: WaterDrop,
};

const Parameter = ({ label, parameterName }) => {
	const Icon = parameterToIcon[parameterName];

	return (
		<Paper
			sx={{
				width: '100%',
				borderRadius: '10px',
				display: 'flex',
				alignItems: 'center',
			}}
		>
			<Grid container spacing={2}>
				<Grid item xs={2}>
					<Icon />
				</Grid>
				<Grid item xs={10}>
					<Typography variant="button">{label}</Typography>
				</Grid>
			</Grid>
		</Paper>
	);
};

/*
    {
        windDirection: number,
        windSpeed: number,
        himidity: number,
    }
*/

const WeatherParameters = ({ windDirection, windSpeed, humidity }) => {
	const parameters = {
		windDirection,
		windSpeed,
		humidity,
	};

	return (
		<Grid item container xs={12} spacing={2}>
			{Object.entries(parameters).map(
				([parameterName, parameterValue], i) => {
					return (
						<Grid item xs={4} key={i}>
							<Parameter
								label={parameterValue}
								parameterName={parameterName}
							/>
						</Grid>
					);
				}
			)}
		</Grid>
	);
};

export default WeatherParameters;
