import React from 'react';
import Typography from '@mui/material/Typography';

function CityName(props) {
	return <Typography variant="body1">{props.name}</Typography>;
}

export default CityName;
