import React from 'react';
import axios from 'axios';
import Paper from '@mui/material/Paper';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import CityName from './CityName';
import Temperature from './Temperature';
import WeatherType from './WeatherType';
import WeatherParameters from './WeatherParameter';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';

export const NA = 'N/A';

function ProfilePage(props) {
	const [state, setState] = React.useState({});
	const [city, setCity] = React.useState('Tyumen');

	const onClick = () => {
		const API_KEY = '98c261e321784352ac6182713240504';

		axios({
			method: 'GET',
			url: `http://api.weatherapi.com/v1/current.json?q=${city}&key=${API_KEY}`,
		}).then(({ data }) => {
			setState(data);
		});
	};

	return (
		<Grid
			container
			spacing={1}
			component={Paper}
			sx={{ padding: '15px', width: 0.35 }}
		>
			<Grid xs={10} item>
				<TextField
					value={city}
					onChange={(e) => {
						setCity(e.target.value);
					}}
					size="small"
					fullWidth
					placeholder="Type, please, your city"
				/>
			</Grid>
			<Grid xs={2} item>
				<Button variant="contained" onClick={onClick} fullWidth>
					Search
				</Button>
			</Grid>
			<Grid item xs={6}>
				<Stack>
					<CityName name={state?.location?.name ?? NA} />
					<Temperature temp={state.current?.temp_c ?? NA} />
				</Stack>
			</Grid>
			<Grid item xs={6}>
				<WeatherType
					type={state.current?.condition?.text ?? NA}
					icon={state?.current?.condition?.icon ?? ''}
				/>
			</Grid>
			<Grid item xs={12}>
				<WeatherParameters
					windDirection={state?.current?.wind_dir ?? NA}
					windSpeed={state?.current?.wind_kph ?? NA}
					humidity={state?.current?.humidity ?? NA}
				/>
			</Grid>
		</Grid>
	);
}

export default ProfilePage;
