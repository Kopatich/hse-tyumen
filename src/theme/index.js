import { createTheme } from '@mui/material/styles';
import { purple, grey } from '@mui/material/colors';

export const theme = createTheme({
	palette: {
		primary: {
			main: purple[500],
		},
		secondary: {
			main: grey[800],
        },
        // success: 
	},
});
