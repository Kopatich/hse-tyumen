import { createSlice } from '@reduxjs/toolkit';

const INITIAL_STATE = {};

export const weatherSlice = createSlice({
	name: 'weatherSlice',
	initialState: INITIAL_STATE,
	reducers: {
		addCity: (state, action) => {
			const { payload } = action;

			state[payload.name] = payload;
			return state;
		},
		clearCity: (state, action) => {
			delete state[action.payload];
		},
		clearAll: (state, action) => {
			state = {};
			return state;
		},
	},
});

export const { addCity, clearCity, clearAll } = weatherSlice.actions;
export default weatherSlice.reducer;
